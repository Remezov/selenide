import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;

public class NewSelenideSelenoidTest {
    @BeforeMethod
    public void setUp() {
        baseUrl = "http://welcome.technokratos.tilda.ws/";
        browser = "chrome";
//        browser = "firefox";
        startMaximized = true;
        timeout = 10000;
        browserSize = "1028x1080";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("acceptInsecureCerts", true);
        capabilities.setCapability("enableVideo", false);
//        FirefoxOptions options = new FirefoxOptions();
//        capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        ChromeOptions options = new ChromeOptions();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        remote = "http://localhost:4444/wd/hub";
        browserCapabilities = capabilities;
    }

    @Test
    public void testNewTest() throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();

        open(baseUrl);
        Thread.sleep(5000);
        $(new By.ByXPath("//div[contains(@class, 'additional_buttons_but')]"))
                .shouldBe(visible, ofSeconds(10))
                .click();

        switchTo().window(1);
        Thread.sleep(5000);
        softAssert.assertTrue($(new By.ByXPath("//h1[text()='Технокра́тер']"))
                .shouldBe(visible, ofSeconds(10))
                .isDisplayed());
        softAssert.assertAll();
    }

}
