import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.*;

public class NewSelenideTest {
    @BeforeMethod
    public void setUp() {
        baseUrl = "http://welcome.technokratos.tilda.ws/";
        browser = "chrome";
        startMaximized = true;
        timeout = 10000;
        browserSize = "1028x1080";
//        headless = true;
    }

    @Test
    public void testNewTest() throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();

        open(baseUrl);
        Thread.sleep(5000);
        $(new By.ByXPath("//div[contains(@class, 'additional_buttons_but')]"))
                .shouldBe(visible, ofSeconds(10))
                .click();

        switchTo().window(1);
        Thread.sleep(5000);
        softAssert.assertTrue($(new By.ByXPath("//h1[text()='Технокра́тер']"))
                .shouldBe(visible, ofSeconds(10))
                .isDisplayed());
        softAssert.assertAll();
    }
}
